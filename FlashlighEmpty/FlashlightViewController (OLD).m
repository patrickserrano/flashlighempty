//
//  FlashlightViewController.m
//  FlashlighEmpty
//
//  Created by Patrick Serrano on 3/19/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "FlashlightViewController.h"

@interface FlashlightViewController ()

@end

@implementation FlashlightViewController

@synthesize AVSession = _AVSession;

-(void)toggleFlashLightOn
{
    AVCaptureDevice *backCamera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([backCamera isTorchAvailable] && [backCamera isTorchModeSupported:AVCaptureTorchModeOn]) 
    {
        BOOL success = [backCamera lockForConfiguration:nil];
        if (success) 
        {
            [backCamera setTorchMode:AVCaptureTorchModeOn];
            [backCamera unlockForConfiguration];
        }
    }
}

-(void)toggleFlashLightOff
{
    AVCaptureDevice *backCamera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([backCamera isTorchAvailable] && [backCamera isTorchModeSupported:AVCaptureTorchModeOn]) 
    {
        BOOL success = [backCamera lockForConfiguration:nil];
        if (success) 
        {
            [backCamera setTorchMode:AVCaptureTorchModeOff];
            [backCamera unlockForConfiguration];
        }
    }
}

- (void)loadView
{
    [self setView:[[UIView alloc] initWithFrame:[[UIScreen mainScreen] applicationFrame]]];
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    
    if ([device hasTorch] == YES) 
    {
        flashlightButton = [[UIButton alloc] initWithFrame:CGRectMake(0, 120, 320, 98)];
        [flashlightButton setBackgroundImage:[UIImage imageNamed:@"TorchOn.png"] forState:UIControlStateNormal];
        [flashlightButton addTarget:self action:@selector(buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        [[self view] addSubview:flashlightButton];
    } else 
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!" message:@"You do not have a LED flash" delegate:nil cancelButtonTitle:@"Dismiss" otherButtonTitles:@"OK", nil];
        [alert show];
    }
}

-(void)buttonPressed:(UIButton *)button
{
    if (button == flashlightButton) 
    {
        if (flashlightOn == NO) 
        {
            flashlightOn = YES;
            [self toggleFlashLightOn];
            [flashlightButton setBackgroundImage:[UIImage imageNamed:@"TorchOff.png"] forState:UIControlStateNormal];
        } else {
            flashlightOn = NO;
            [self toggleFlashLightOff];
            [flashlightButton setBackgroundImage:[UIImage imageNamed:@"TorchOn.png"] forState:UIControlStateNormal];
        }
    }
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
