//
//  FlashlightViewController.m
//  FlashlighEmpty
//
//  Created by Patrick Serrano on 3/19/12.
//  Copyright (c) 2012 Patrick Serrano. All rights reserved.
//

#import "FlashlightViewController.h"

@interface FlashlightViewController ()

@end

@implementation FlashlightViewController

@synthesize AVSession = _AVSession;

-(void)toggleFlashLightOn
{
    AVCaptureDevice *backCamera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([backCamera isTorchAvailable] && [backCamera isTorchModeSupported:AVCaptureTorchModeOn]) 
    {
        BOOL success = [backCamera lockForConfiguration:nil];
        if (success) 
        {
            [backCamera setTorchMode:AVCaptureTorchModeOn];
            [backCamera unlockForConfiguration];
        }
    }
}

-(void)toggleFlashLightOff
{
    AVCaptureDevice *backCamera = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if ([backCamera isTorchAvailable] && [backCamera isTorchModeSupported:AVCaptureTorchModeOn]) 
    {
        BOOL success = [backCamera lockForConfiguration:nil];
        if (success) 
        {
            [backCamera setTorchMode:AVCaptureTorchModeOff];
            [backCamera unlockForConfiguration];
        }
    } 
}

-(IBAction)changeLabel:(id)sender
{
    if (flashlightOn == NO) 
        {
            flashlightOn = YES;
            [self toggleFlashLightOn];
           
        } else {
            flashlightOn = NO;
            [self toggleFlashLightOff];
        }

}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
