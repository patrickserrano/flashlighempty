//
//  FlashlightAppDelegate.h
//  FlashlighEmpty
//
//  Created by Patrick Serrano on 3/19/12.
//  Copyright (c) 2012 Patrick Serrano. All rights reserved.
//

#import <UIKit/UIKit.h>

@class FlashlightViewController;

@interface FlashlightAppDelegate : NSObject <UIApplicationDelegate, UIImagePickerControllerDelegate>
{
    UIWindow *window;
    FlashlightViewController *vc;
}

@end