//
//  FlashlightAppDelegate.m
//  FlashlighEmpty
//
//  Created by Patrick Serrano on 3/19/12.
//  Copyright (c) 2012 Patrick Serrano. All rights reserved.
//

#import "FlashlightAppDelegate.h"
#import "FlashlightViewController.h"

@implementation FlashlightAppDelegate

-(void)applicationDidFinishLaunching:(UIApplication *)application
{
    window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    // Override point for customization after application launch.
    vc = [[FlashlightViewController alloc] init];
    
    [window addSubview:vc.view];
    [window makeKeyAndVisible];
    
}

@end
