//
//  FlashlightViewController.h
//  FlashlighEmpty
//
//  Created by Patrick Serrano on 3/19/12.
//  Copyright (c) 2012 Patrick Serrano. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface FlashlightViewController : UIViewController
{
    UIButton *flashlightButton;
    AVCaptureSession *AVSession;
    BOOL flashlightOn;
}

@property (nonatomic, strong) AVCaptureSession *AVSession;

-(IBAction)changeLabel:(id)sender;

@end
